from .event import Event3

class SayEvent(Event3):
    NAME = "say"

    def perform(self):
        self.add_prop("said-"+self.object2)
        self.buffer_inform("say.actor", props=self._get_props())
        # for prop in self.get_props():
        self.inform("say")
