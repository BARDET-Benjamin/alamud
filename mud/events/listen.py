# -*- coding: utf-8 -*-
from .event import Event2

class ListenEvent(Event2):
    NAME = "listen"

    def perform(self):
        if self.object.has_prop("object-not-listenable"):
            return self.listen_failed()
        self.inform("listen")

    def listen_failed(self):
        self.fail()
        self.inform("listen.failed")
