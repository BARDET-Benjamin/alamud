from .event import Event3

class PutOnEvent(Event3):
    NAME = "put-on"

    def perform(self):
        if not self.object2.is_container():
            self.add_prop("object2-not-container")
            return self.put_in_failure()
        if self.object not in self.actor:
            self.add_prop("object-not-in-inventory")
            return self.put_in_failure()
        if not self.get_datum("put-on.data-driven"):
            self.object.move_to(self.object2)
        self.inform("put-on")

    def put_in_failure(self):
        self.fail()
        self.inform("put-on.failed")
