# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .eat       import EatAction
from .go        import GoAction, LeaveAction, EnterAction
from .inspect   import InspectAction
from .look      import LookAction
from .open      import OpenAction, OpenWithAction
from .close     import CloseAction
from .type      import TypeAction
from .take      import TakeAction
from .inventory import InventoryAction
from .light     import LightOnAction, LightOffAction, LightOnWithAction
from .drop      import DropAction, DropInAction
from .push      import PushAction
from .teleport  import TeleportAction
from .burn      import BurnWithAction
from .listen    import ListenAction
from .fire      import FireAction
from .put       import PutOnAction
# from .say       import SayAction
